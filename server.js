'use strict';

// var io     = require("socket.io"),
//     http   = require('http'),
//     server = http.createServer(),
//     io     = io.listen(server);

const express = require('express');
const socketIO = require('socket.io');
const path = require('path');

const PORT = process.env.PORT || 3000;
const INDEX = path.join(__dirname, 'index.html');

const server = express()
  .use(function(req, res) {
    res.sendFile(INDEX)
  })
  .listen(PORT, function() {
    console.log(`Listening on ${ PORT }`)
  });

const io = socketIO(server);

setInterval(function() {
  io.emit('time', new Date().toTimeString()),
  60000
});



io.on('connection', function(socket){
  console.log("User Connected");

  socket.on("message", function(message){
    io.emit("message", message);
  });

  socket.on('disconnect', function() {
    console.log('Client disconnected')
  });
});

server.listen(4000, function(){
  console.log("server started");
});